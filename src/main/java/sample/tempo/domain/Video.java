package sample.tempo.domain;

/**
 * Copyright 2018/12 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * <a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

public record Video(String title, boolean isUpgrade) implements Physical {
    public static final String TARGET_TITLE = "Learning to Ski";

    public Video {
         title = title != null ? title : TARGET_TITLE;
    }
  public static class Builder {
      String title;
      boolean isUpgrade;
      public Builder(String title) {
        this.title = title;
      }

    public Builder isUpgrade(boolean isUpgrade) {
        this.isUpgrade = isUpgrade;
        return this;
    }

    public Video build() {
        return new Video(title, false);
    }
  }
}
