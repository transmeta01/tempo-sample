package sample.tempo.rules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Rule;
import sample.tempo.domain.Membership;

/**
 * Copyright 2018/12 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

@Rule(name = "rule for membership", description = "", priority = 3)
public record MembershipRule(Membership membership) {
    @Condition
    public boolean when() {
        return this.membership.isUpgrade();
    }

    @Action
    public void then() {
        System.out.println( "apply upgrade to membership" );
    }
}
