package sample.tempo.domain;

import java.util.List;
import java.util.Objects;

/**
 * Copyright 2018/12 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * <a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

public record Payment(List<Product> products) {
  public Payment {
    Objects.requireNonNull(products, "One of more products is/are required for the payment");
  }
}
