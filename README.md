# README #

This is a sample of the test from tempo. It demonstrate a very basic use of a rule engine to solve the problem 
stated here : http://codekata.com/kata/kata16-business-rules/

### What is this repository for? ###

* Tempo sample test
* Version : 1.00-SNAPSHOT

### How do I get set up? ###

* To view source import mvn project into your favorite IDE 
* To run execute : mvn compile exec:java -Dexec.mainClass=sample.tempo.Launcher
* Dependencies : see pom.xml file

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact