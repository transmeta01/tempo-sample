package domain

/**
 * Copyright 2018/12 www.cogalab.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 *
 */

sealed class Product

data class Payment(val type: Product)

sealed class Physical : Product() {
    abstract val title : String
}

data class Book(override val title : String) : Physical()
data class Video(override val title : String): Physical()
data class Membership(val isUpgrade: Boolean, val isActivated: Boolean) : Product()

