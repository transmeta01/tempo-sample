package sample.tempo;

/**
 * Copyright 2018/12 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import sample.tempo.domain.Book;
import sample.tempo.domain.Membership;
import sample.tempo.domain.Video;
import sample.tempo.rules.BookRule;
import sample.tempo.rules.MembershipRule;
import sample.tempo.rules.PhysicalProductRule;

public class Launcher {
  public static void main(String[] args) {
    // generate facts
    Book book = new Book.Builder("the zen on motorcycle maintenance").build();
    Video video = new Video.Builder(null).build();

    // create facts
    Facts facts = new Facts();
    facts.put("book", book);

    // create rules
    Rules rules = new Rules();
    rules.register(new PhysicalProductRule(video));
    rules.register(new MembershipRule(new Membership(true, false)));
    rules.register(new BookRule(book));

    // create a rules engine and fire rules on known facts
    RulesEngine rulesEngine = new DefaultRulesEngine();
    rulesEngine.fire(rules, facts);
  }
}
